package com.example.demo.controller;

import com.example.demo.entity.ReqquestObj;
import com.example.demo.services.DemoServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api")
public class DemoController {
    @Autowired
    DemoServices demoServices;
    @GetMapping("/demo/{hungvan}")
    public String hello(@PathVariable String hungvan){
        return "hello" + hungvan;
    }

    @PostMapping("/login")
    public String login(@RequestBody ReqquestObj obj){
        return demoServices.accountant(obj);
    }
}
