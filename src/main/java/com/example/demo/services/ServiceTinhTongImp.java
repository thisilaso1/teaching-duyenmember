package com.example.demo.services;

import com.example.demo.entity.tinhTong;
import org.springframework.stereotype.Service;

@Service
public class ServiceTinhTongImp implements ServicesTinhTong {

    @Override
    public int tong(tinhTong sum) {
        int hung = sum.getA() +sum.getB();
        return hung;
    }
}
